
const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/login',
    component: () => import('layouts/AuthLayout.vue'),
  },
  {
    path: '/errorauth',
    component: () => import('pages/ErrorUnauthorized.vue'),
  },
  {
    path: '/home',
    component: () => import('layouts/NewHomeLayout.vue'),
    children: [
      { path: '', components: {
        helper :  () => import('pages/Index.vue') },
      }
    ]
  },
  {
    path: '/mutasistock',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        mutasistock: () => import('layouts/ReportLayout.vue') },
      }
    ]
  },
  {
    path: '/mutasistockincoming',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        mutasistock: () => import('pages/Report/Mutasi/Pemasukan.vue') },
      }
    ]
  },
  {
    path: '/mutasistockoutgoing',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        mutasistock: () => import('pages/Report/Mutasi/Pengeluaran.vue') },
      }
    ]
  },
  {
    path: '/wip',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        wip: () => import('pages/Report/Pabean/WIP.vue') },
      }
    ]
  },
  {
    path: '/logs',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        logs: () => import('pages/Logs/UserLogs.vue') },
      }
    ]
  },
  {
    path: '/wmslogs',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        logs: () => import('pages/Logs/WMSUserLogs.vue') },
      }
    ]
  },
  {
    path: '/bahanbaku',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        bahanbaku: () => import('pages/Report/Pabean/BahanBaku.vue') },
      }
    ]
  },
  {
    path: '/barangjadi',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        barangjadi: () => import('pages/Report/Pabean/BarangJadi.vue') },
      }
    ]
  },
  {
    path: '/reject',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        reject: () => import('pages/Report/Pabean/Reject.vue') },
      }
    ]
  },
  {
    path: '/mesin',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        mesin: () => import('pages/Report/Pabean/MesinAlat.vue') },
      }
    ]
  },
  {
    path: '/kitjob',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        kitjob :  () => import('pages/Report/KitingJob/index.vue') },
      }
    ]
  },
  {
    path: '/scrapreport',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        scrapreport :  () => import('pages/Transactions/scrapReportCreate') },
      }
    ]
  },
  {
    path: '/prodesignmaster',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        prodesignmaster :  () => import('pages/Transactions/designProcess') },
      }
    ]
  },
  {
    path: '/userreg',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        userreg: () => import('pages/Report/Settings/UserReg.vue') },
      }
    ]
  },
  {
    path: '/role',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        role: () => import('pages/Report/Settings/Role.vue') },
      }
    ]
  },
  {
    path: '/menu',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        menu: () => import('pages/Report/Settings/Menu.vue') },
      }
    ]
  },
  {
    path: '/upscrap',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        upscrap: () => import('pages/Transactions/UploadScrap.vue') },
      }
    ]
  },
  {
    path: '/upmesin',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', components: {
        upmesin: () => import('pages/Transactions/UploadMesin.vue') },
      }
    ]
  },
]

// this.$axios.get('wmsreport.test:85/api/inventory/getmenuall')
// .then(val => {
//   val.forEach(hasil => {
//     this.routes.push({
//       path : hasil.MENU_URL,
//       component:
//     })
//   });
// })

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
