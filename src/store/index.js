import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

import AuthStore from './AuthStore'
// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const vuexPersist = new VuexPersist({
    key: 'vuexDatabase',
    storage: window.localStorage
  })
  
  const Store = new Vuex.Store({
    modules: {
      AuthStore
      // example
    },
    plugins: [vuexPersist.plugin],
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
