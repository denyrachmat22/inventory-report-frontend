import { QSpinnerFacebook } from "quasar";

export const HelpersComponent = {
  data() {
    return {
      data: "",
      condition: [
        {
          value: "contain",
          label: "Contains"
        },
        {
          value: "equals",
          label: "="
        },
        {
          value: "range",
          label: "Range"
        },
        {
          value: "less_than_equal",
          label: "=<"
        }
      ],
      menuChoose: []
    };
  },
  created() {
    // this.cekValiditasUserPage(this.$route.path)
  },
  methods: {
    postData(opt, request = null) {
      if (opt.loading) {
        this.$q.loading.show({
          spinner: QSpinnerFacebook,
          spinnerSize: 140,
          backgroundColor: "cyan",
          message: "Some important process is in progress. Hang on...",
          messageColor: "black"
        });
      }

      if (request) {
        var header = this.$axios[opt.methods](this.mainUrl + opt.url, request);
      } else {
        var header = this.$axios[opt.methods](this.mainUrl + opt.url);
      }

      let req = header
        .then(res => {
          if (opt.loading) {
            this.$q.loading.hide();
          }

          return res.data;
        })
        .catch(e => {
          if (opt.loading) {
            this.$q.loading.hide();
          }

          if (e.response.status == 422) {
            let errors = e.response.data.errors;
            Object.keys(errors).map(val => {
              errors[val].map(val_det => {
                this.$q.notify({
                  color: "negative",
                  message: val_det
                });
              });
            });
          }
        });

      return req;
    },
    blobParsing(Blob, Filename) {
      Blob.lastModifiedDate = new Date();
      Blob.name = Filename;
      return Blob;
    },
    resetForm() {},
    cekValiditasUserPage(url) {
      if (url == "/home" || url == "/login") {
        console.log("ini home");
      } else {
        let test2 = this.authState.data.parent.filter(x =>
          x.child.some(y => "/" + y.MENU_URL == url)
        );

        if (test2.length == 0) {
          this.$router.push("errorauth");
        }
      }
    },
    parsingtoreadabledate (datestr) {
      const date = new Date(datestr)
      const dateTimeFormat = new Intl.DateTimeFormat('en', {
        year: 'numeric',
        month: 'short',
        day: '2-digit'
      })
      const [
        { value: month },
        ,
        { value: day },
        ,
        { value: year }
      ] = dateTimeFormat.formatToParts(date)

      return `${day}-${month}-${year}`
    },
    parsingisotodatesql (datestr, monthonly = null) {
      const newdate = new Date(datestr)
      var year = newdate.getFullYear()
      var month = newdate.getMonth() + 1
      var dt = newdate.getDate()

      if (dt < 10) {
        dt = '0' + dt
      }
      if (month < 10) {
        month = '0' + month
      }

      if (monthonly) {
        return month + '-' + year
      } else {
        return year + '-' + month + '-' + dt + ' ' + newdate.toLocaleTimeString()
      }
    },
  },
  computed: {
    authState: {
      get() {
        let storage = JSON.parse(localStorage.getItem("vuexDatabase"));
        if (storage) {
          return storage.AuthStore.authDet;
        } else {
          return null;
        }
      }
    },
    mainUrl: {
      get() {
        let storage = JSON.parse(localStorage.getItem("vuexDatabase"));
        if (storage) {
          return storage.AuthStore.MainURL;
        } else {
          console.log(this.$store.state.AuthStore);
          return this.$store.state.AuthStore.MainURL;
        }
      }
    }
  }
  // watch: {
  //   '$router.path' (to, from) {
  //     console.log(to);

  //     this.cekValiditasUserPage(to)
  //   }
  // },
};
