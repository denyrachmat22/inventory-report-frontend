export default [
  {
    name: "PemasukanPabean",
    form: [
      {
        field: {
          name: "tglpabean",
          required: true,
          label: "Tgl Pabean",
          field: "RPBEA_TGLBEA",
          value: "RPBEA_TGLBEA",
          type: "date"
        },
        op: "range",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "RCV_BCTYPE",
          required: true,
          label: "Jenis Pabean",
          field: "RPBEA_JENBEA",
          value: "RPBEA_JENBEA",
          type: "select",
          selectOpt: [
            {
              label: "BC 1.0",
              value: "10"
            },
            {
              label: "BC 1.1",
              value: "11"
            },
            {
              label: "BC 1.2",
              value: "12"
            },
            {
              label: "BC 1.3",
              value: "13"
            },
            {
              label: "BC 2.0",
              value: "20"
            },
            {
              label: "BC 2.1",
              value: "21"
            },
            {
              label: "BC 2.2",
              value: "22"
            },
            {
              label: "BC 2.3",
              value: "23"
            },
            {
              label: "BC 2.4",
              value: "24"
            },
            {
              label: "BC 2.5",
              value: "25"
            },
            {
              label: "BC 2.6.1",
              value: "261"
            },
            {
              label: "BC 2.6.2",
              value: "262"
            },
            {
              label: "BC 2.7",
              value: "27"
            },
            {
              label: "BC 3.0",
              value: "30"
            },
            {
              label: "BC 3.2",
              value: "32"
            },
            {
              label: "BC 4.0",
              value: "40"
            }
          ]
        },
        op: "equals",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true,
        valselect: []
      },
      {
        field: {
          name: "RCV_BCNO",
          required: true,
          label: "No Pabean",
          field: "RPBEA_NUMBEA",
          value: "RPBEA_NUMBEA",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "kodebrg",
          required: true,
          label: "Kode Barang",
          field: "RPBEA_ITMCOD",
          value: "RPBEA_ITMCOD",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "nmbrg",
          required: true,
          label: "Name Barang",
          field: "MITM_ITMD1",
          value: "MITM_ITMD1",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      }
    ]
  },
  {
    name: "PengeluaranPabean",
    form: [
      {
        field: {
          name: "tglpabean",
          required: true,
          label: "Tgl Pabean",
          field: "RPBEA_TGLBEA",
          value: "RPBEA_TGLBEA",
          type: "date"
        },
        op: "range",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "RCV_BCTYPE",
          required: true,
          label: "Jenis Pabean",
          field: "RPBEA_JENBEA",
          value: "RPBEA_JENBEA",
          type: "select",
          selectOpt: [
            {
              label: "BC 1.0",
              value: "10"
            },
            {
              label: "BC 1.1",
              value: "11"
            },
            {
              label: "BC 1.2",
              value: "12"
            },
            {
              label: "BC 1.3",
              value: "13"
            },
            {
              label: "BC 2.0",
              value: "20"
            },
            {
              label: "BC 2.1",
              value: "21"
            },
            {
              label: "BC 2.2",
              value: "22"
            },
            {
              label: "BC 2.3",
              value: "23"
            },
            {
              label: "BC 2.4",
              value: "24"
            },
            {
              label: "BC 2.5",
              value: "25"
            },
            {
              label: "BC 2.6.1",
              value: "261"
            },
            {
              label: "BC 2.6.2",
              value: "262"
            },
            {
              label: "BC 2.7",
              value: "27"
            },
            {
              label: "BC 3.0",
              value: "30"
            },
            {
              label: "BC 3.2",
              value: "32"
            },
            {
              label: "BC 4.1",
              value: "41"
            }
          ]
        },
        op: "equals",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true,
        valselect: []
      },
      {
        field: {
          name: "RCV_BCNO",
          required: true,
          label: "No Pabean",
          field: "RPBEA_NUMBEA",
          value: "RPBEA_NUMBEA",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "kodebrg",
          required: true,
          label: "Kode Barang",
          field: "RPBEA_ITMCOD",
          value: "RPBEA_ITMCOD",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "nmbrg",
          required: true,
          label: "Name Barang",
          field: "MITM_ITMD1",
          value: "MITM_ITMD1",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      }
    ]
  },
  {
    name: "WIP",
    form: [
      {
        field: {
          label: "Tanggal",
          field: "RPWIP_DATEIS",
          value: "RPWIP_DATEIS",
          type: "date"
        },
        op: "less_than_equal",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          label: "Kode Barang",
          field: "RPWIP_ITMCOD",
          value: "RPWIP_ITMCOD",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "nmbrg",
          required: true,
          label: "Name Barang",
          field: "MITM_ITMD1",
          value: "MITM_ITMD1",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      }
    ]
  },
  {
    name: "BahanBaku",
    form: [
      {
        field: {
          label: "Tanggal",
          field: "RPRAW_DATEIS",
          value: "RPRAW_DATEIS",
          type: "month"
        },
        op: "range",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          label: "Kode Barang",
          field: "RPRAW_ITMCOD",
          value: "RPRAW_ITMCOD",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "nmbrg",
          required: true,
          label: "Name Barang",
          field: "MITM_ITMD1",
          value: "MITM_ITMD1",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      }
    ]
  },
  {
    name: "FinishGood",
    form: [
      {
        field: {
          label: "Tanggal",
          field: "RPGOOD_DATEIS",
          value: "RPGOOD_DATEIS",
          type: "month"
        },
        op: "range",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          label: "Kode Barang",
          field: "RPGOOD_ITMCOD",
          value: "RPGOOD_ITMCOD",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "nmbrg",
          required: true,
          label: "Name Barang",
          field: "MITM_ITMD1",
          value: "MITM_ITMD1",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      }
    ]
  },
  {
    name: "Scrap",
    form: [
      {
        field: {
          label: "Tanggal",
          field: "RPREJECT_DATEIS",
          value: "RPREJECT_DATEIS",
          type: "month"
        },
        op: "range",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          label: "Kode Barang",
          field: "RPREJECT_ITMCOD",
          value: "RPREJECT_ITMCOD",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "nmbrg",
          required: true,
          label: "Name Barang",
          field: "MITM_ITMD1",
          value: "MITM_ITMD1",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      }
    ]
  },
  {
    name: "MesinAlat",
    form: [
      {
        field: {
          label: "Tanggal",
          field: "RPMCH_DATEIS",
          value: "RPMCH_DATEIS",
          type: "month"
        },
        op: "range",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          label: "Kode Barang",
          field: "RPMCH_ITMCOD",
          value: "RPMCH_ITMCOD",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      },
      {
        field: {
          name: "nmbrg",
          required: true,
          label: "Name Barang",
          field: "MITM_ITMD1",
          value: "MITM_ITMD1",
          type: "string"
        },
        op: "contain",
        val: "",
        val2: "",
        readonlyop: true,
        readonlyfield: true
      }
    ]
  }
];
